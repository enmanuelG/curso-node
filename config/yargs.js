const argv = require('yargs')
            .option('b',{
                alias: 'base',
                type: 'number',
                demandOption: true,
                description: 'numero para la multiplicacion'
            })
            .option('l',{
                alias: 'listar',
                type: 'boolean',
                demandOption: true,
                description: 'opcion para mostrar o no la tabla',
                default: false
            })
            .option('h',{
                alias: 'hasta',
                type: 'number',
                demandOption: true,
                description: 'aqui selecionamos hasta que se multiplicaria',
                default: 20
            })    
            .check( (argv, options) => {
                if(isNaN(argv.b)){
                    throw 'la Base tiene que ser un numero'
                }
                return true;
            })
            .argv;

module.exports = argv;