const fs  = require('fs');

const crearArchivo = async (tabla = 5, lis = false, hasta = 20) => {
    
    try {

       

        let  salida, consola = '';
        for (let index = 1; index <= hasta; index++) {
            salida += ` ${tabla} ${'x'.blue} ${index} : ${tabla * index} \n` ;
            consola += ` ${tabla} x ${index} : ${tabla * index} \n` ;
        }

        fs.writeFile(`tabla-${tabla}.txt`, consola, (err)=>{
            if(err) throw err;

            // console.log('archivo creado correctamente')
        }) 

        if(lis){
            console.log('============================================');
            console.log(`tabla de multiplicar del numero ${tabla} : `);
            console.log('==============================================');

            console.log(salida);
        }
           

    } catch (error) {
        throw error;
    }

    
    return `archivo ${tabla} creado`;
}


module.exports = {
    crearArchivo
}